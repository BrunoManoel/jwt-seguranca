package com.aula.itau.seguranca.services;

import com.aula.itau.seguranca.auth.AuthUsuario;
import com.aula.itau.seguranca.models.Usuario;
import com.aula.itau.seguranca.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;

    public Usuario salvarUsuario(Usuario usuario){
        String senha = usuario.getSenha();

        usuario.setSenha(encoder.encode(senha));

        return usuarioRepository.save(usuario);
    }

    public Iterable<Usuario> lerTodosOsUsuarios(){
        return usuarioRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario == null ){
            throw new UsernameNotFoundException("Email não cadastrado");
        }
        AuthUsuario authUsuario = new AuthUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return authUsuario;
    }
}